param(
    [parameter(mandatory=$true)]
    $path
)

$commands = gc $path | % {
    if ($_ -match '(?<command>forward|up|down) (?<amt>\d+)') {
        [pscustomobject] @{
            command = $Matches["command"];
            amt = [int]$Matches["amt"];
        }
    }
}

echo "question 1"

$position = 0
$depth = 0

foreach ($c in $commands) {
    switch ($c.command) {
        "forward" { $position += $c.amt }
        "up"      { $depth -= $c.amt }
        "down"    { $depth += $c.amt }
    }
}

echo "position: $position, depth: $depth, answer: $($position * $depth)"

echo "question 2"

$position = 0
$depth = 0
$aim = 0

foreach ($c in $commands) {
    switch ($c.command) {
        "forward" { $position += $c.amt; $depth += $aim * $c.amt }
        "up"      { $aim -= $c.amt }
        "down"    { $aim += $c.amt }
    }
}
echo "position: $position, depth: $depth, answer: $($position * $depth)"
