param(
    [parameter(mandatory=$true)]
    $path
)

$nums = gc $path | % { [int]$_ }

echo "First question:"
$last = $nums[0]
$nums[1..($nums.count)] | % {
    if ($last -lt $_) { 
        $_
    }
    $last = $_
} | measure-object | % Count

echo "Second question:"

$last = $nums[0] + $nums[1] + $nums[2]
1..($nums.count-3) | % {
    $x = $nums[$_] + $nums[$_+1] + $nums[$_+2]
    if ($last -lt $x) {
        $x
    }
    $last = $x
} | measure-object | % count
